package io.cleaner.app.screens.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import io.cleaner.app.APP_ONBOARDING
import io.cleaner.app.BaseActivity
import io.cleaner.app.R
import io.cleaner.app.databinding.ActivityHomeBinding
import io.cleaner.app.databinding.ActivityOnboardingBinding
import java.lang.NullPointerException

class OnboardingActivity : BaseActivity() {

    private var binding: ActivityOnboardingBinding?= null
    private val mBinding get() = binding!!
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {
        }
        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        APP_ONBOARDING = this
        navController = Navigation.findNavController(this, R.id.nav_host_fragment_onboarding)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}