package io.cleaner.app.screens.fragments.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.cleaner.app.APP_ONBOARDING
import io.cleaner.app.BaseFragment
import io.cleaner.app.R
import io.cleaner.app.databinding.FragmentFourBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class FourFragment : BaseFragment() {

    private var binding: FragmentFourBinding?= null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFourBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initial()
    }

    private fun initial() {

        var isCheck = true
        mBinding.imgPlan1.setOnClickListener {
            if (isCheck){
                isCheck = false
                mBinding.imgPlan1.setImageResource(R.drawable.plan1_2)
                mBinding.imgPlan2.setImageResource(R.drawable.plan2_2)
            }
        }
        mBinding.imgPlan2.setOnClickListener {
            if (!isCheck){
                isCheck = true
                mBinding.imgPlan1.setImageResource(R.drawable.plan1_1)
                mBinding.imgPlan2.setImageResource(R.drawable.plan2_1)
            }
        }

        mBinding.btnChoice.setOnClickListener {
            APP_ONBOARDING.navController.navigate(R.id.action_fourFragment_to_mainActivity)
        }
        mBinding.contWithoutSub.setOnClickListener {
            APP_ONBOARDING.navController.navigate(R.id.action_fourFragment_to_mainActivity)
        }

        mBinding.privacyPolicy.setOnClickListener {
            APP_ONBOARDING.navController.navigate(R.id.action_fourFragment_to_privacyPolicyFragment)
        }

        mBinding.tvAgreement.setOnClickListener {
            APP_ONBOARDING.navController.navigate(R.id.action_fourFragment_to_privacyPolicyFragment)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }


}