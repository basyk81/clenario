package io.cleaner.app.screens.fragments.onboarding

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import io.cleaner.app.APP_HOME
import io.cleaner.app.APP_ONBOARDING
import io.cleaner.app.BaseFragment
import io.cleaner.app.R
import io.cleaner.app.adapters.OnboardAdapter
import io.cleaner.app.databinding.FragmentRootOnboardingBinding


class RootOnboardingFragment : BaseFragment() {
    private val NUM_PAGES = 4
    private var dots: List<ImageView>? = null
    private var binding: FragmentRootOnboardingBinding?= null
    private val mBinding get() = binding!!
    var runnable: Runnable? = null
    var handler: Handler? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRootOnboardingBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.vpOnboarding.adapter = OnboardAdapter(ctx as FragmentActivity)
        addDots()
        init()
    }

    private fun init() {
        mBinding.tvSkip.setOnClickListener {
            APP_ONBOARDING.navController.navigate(R.id.action_rootOnboardingFragment_to_fourFragment)
            stopTask()
        }
        handler = Handler()
        runnable = Runnable {
            if (mBinding.vpOnboarding.currentItem<2) {
                mBinding.vpOnboarding.currentItem = mBinding.vpOnboarding.currentItem + 1
            }else{
                APP_ONBOARDING.navController.navigate(R.id.action_rootOnboardingFragment_to_fourFragment)
                stopTask()
            }  }
        mBinding.btnNextPage.setOnClickListener {
            if (mBinding.vpOnboarding.currentItem<2) {
                mBinding.vpOnboarding.currentItem = mBinding.vpOnboarding.currentItem + 1
            }else{
                APP_ONBOARDING.navController.navigate(R.id.action_rootOnboardingFragment_to_fourFragment)
               stopTask()

            }
        }
        requireActivity()
            .onBackPressedDispatcher
            .addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (isEnabled) {
                        isEnabled = false
                        requireActivity().onBackPressed()
                        stopTask()
                    }
                }
            }
            )


    }

    fun addDots() {
        dots = ArrayList()
        for (i in 0 until NUM_PAGES) {
            val dot = ImageView(requireActivity())
            dot.setImageDrawable(resources.getDrawable(R.drawable.pager_dot_not_selected))
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            dot.setPadding(5)
            mBinding.dots.addView(dot, params)
            (dots as ArrayList<ImageView>).add(dot)

        }
        mBinding.vpOnboarding.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageSelected(position: Int) {
                selectDot(position)
                super.onPageSelected(position)
                if (position>=2){
                    binding!!.tvSkip.visibility = View.INVISIBLE
                }
                    // handler!!.postDelayed(runnable!!, 7000)
            }

            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
            }
        })
    }
    fun selectDot(idx: Int) {
        val res= resources
        for (i in 0 until NUM_PAGES) {
            val drawableId: Int =
                if (i <= idx) R.drawable.pager_dot_selected else R.drawable.pager_dot_not_selected
            val drawable: Drawable = res.getDrawable(drawableId)
            dots!!.get(i).setImageDrawable(drawable)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }



  fun stopTask() {
          handler!!.removeCallbacks(runnable!!);
          handler!!.removeCallbacksAndMessages(null);



  }

}