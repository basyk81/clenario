package io.cleaner.app.screens.fragments.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.cleaner.app.APP_ONBOARDING
import io.cleaner.app.R
import io.cleaner.app.databinding.FragmentPrivacyPolicyBinding


class PrivacyPolicyFragment : Fragment() {

    private var binding: FragmentPrivacyPolicyBinding?= null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPrivacyPolicyBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.btnAccept.setOnClickListener {
            APP_ONBOARDING.navController.navigate(R.id.action_privacyPolicyFragment_to_fourFragment)
        }
        mBinding.btnBack.setOnClickListener {
            APP_ONBOARDING.navController.navigate(R.id.action_privacyPolicyFragment_to_fourFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }


}