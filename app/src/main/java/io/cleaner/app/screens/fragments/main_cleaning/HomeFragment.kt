package io.cleaner.app.screens.fragments.main_cleaning

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import io.cleaner.app.APP_HOME
import io.cleaner.app.R
import io.cleaner.app.databinding.FragmentHomeBinding
import android.app.Activity
import androidx.appcompat.app.ActionBarDrawerToggle

import io.cleaner.app.BaseFragment


class HomeFragment : BaseFragment() {

    private var binding: FragmentHomeBinding?= null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        mBinding.btnDrawer.setOnClickListener {
            mBinding.drawerLayout.open()
        }

        mBinding.navView.setNavigationItemSelectedListener { menuItem ->
//            if (menuItem.itemId == R.id.p){
//                APP_HOME.navController.navigate(R.id.action_homeFragment_to_rootOnboardingFragment)
//            }
            menuItem.isChecked = true
            mBinding.drawerLayout.close()
            true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }


}