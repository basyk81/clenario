package io.cleaner.app.screens.fragments.firstloading

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.cleaner.app.BaseFragment
import io.cleaner.app.R
import io.cleaner.app.databinding.FragmentEndLoadBinding
import io.cleaner.app.databinding.FragmentFirstLoadingBinding
import android.text.TextUtils
import io.cleaner.app.APP_MAIN


class EndLoadFragment : BaseFragment() {
    private var binding: FragmentEndLoadBinding?= null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEndLoadBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        val deviceName = Build.MANUFACTURER + " " + Build.MODEL
        mBinding.lottieListScan.frame = 160
        mBinding.tvNamePhone.text = deviceName

        mBinding.btnContenue.setOnClickListener {
            APP_MAIN.navController.navigate(R.id.action_endLoadFragment_to_homeActivity)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}