package io.cleaner.app.screens.activities

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import io.cleaner.app.APP_MAIN
import io.cleaner.app.BaseActivity
import io.cleaner.app.R
import io.cleaner.app.databinding.ActivityMainBinding
import java.lang.NullPointerException

class MainActivity : BaseActivity() {

    private var binding: ActivityMainBinding?= null
    private val mBinding get() = binding!!
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        APP_MAIN = this
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}