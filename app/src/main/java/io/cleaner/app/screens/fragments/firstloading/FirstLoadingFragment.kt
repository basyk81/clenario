package io.cleaner.app.screens.fragments.firstloading

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import io.cleaner.app.APP_MAIN
import io.cleaner.app.BaseFragment
import io.cleaner.app.R
import io.cleaner.app.REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION
import io.cleaner.app.databinding.FragmentFirstLoadingBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class FirstLoadingFragment : BaseFragment() {

    private var binding: FragmentFirstLoadingBinding?= null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirstLoadingBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        initAnim()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                   // startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                    CoroutineScope(Dispatchers.Main).launch{
                        mBinding.btnContenue.isVisible = false

                        mBinding.lottieScanning.resumeAnimation()
                        mBinding.lottieListScan.resumeAnimation()
                        delay(2500)
                        APP_MAIN.navController.navigate(R.id.action_firstLoadingFragment_to_endLoadFragment)
                    }
                } else {
                    CoroutineScope(Dispatchers.Main).launch{
                        mBinding.btnContenue.isVisible = false

                        mBinding.lottieScanning.resumeAnimation()
                        mBinding.lottieListScan.resumeAnimation()
                        delay(2500)
                        APP_MAIN.navController.navigate(R.id.action_firstLoadingFragment_to_endLoadFragment)
                    }
                }
                return
            }
        }
    }

    private fun initAnim() {

        mBinding.view.visibility = View.INVISIBLE
        CoroutineScope(Dispatchers.Main).launch {

            mBinding.lottieScanning.isVisible = true
            mBinding.lottieListScan.isVisible = true
            mBinding.view.visibility = View.VISIBLE

            // play animation
            mBinding.lottieScanning.playAnimation()
            mBinding.lottieListScan.playAnimation()
            mBinding.lottieListScan.speed = 0.5f
            delay(6000)

            //pause animation
            mBinding.lottieScanning.pauseAnimation()
            mBinding.lottieListScan.pauseAnimation()

            val writeExternalStoragePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
            when {
                ContextCompat.checkSelfPermission(requireContext(), writeExternalStoragePermission) == PackageManager.PERMISSION_GRANTED ->     CoroutineScope(Dispatchers.Main).launch{
                    mBinding.btnContenue.isVisible = false
                    mBinding.lottieScanning.resumeAnimation()
                    mBinding.lottieListScan.resumeAnimation()
                    delay(4000)
                    APP_MAIN.navController.navigate(R.id.action_firstLoadingFragment_to_endLoadFragment)
                }
                else -> requestPermissions(arrayOf(writeExternalStoragePermission), REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION)
            }
            addButtonPerm()
        }




        mBinding.btnContenue.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch{
                mBinding.btnContenue.isVisible = false

                mBinding.lottieScanning.resumeAnimation()
                mBinding.lottieListScan.resumeAnimation()
                delay(2500)
                APP_MAIN.navController.navigate(R.id.action_firstLoadingFragment_to_endLoadFragment)
            }

        }
    }
    private fun addButtonPerm() {
        CoroutineScope(Dispatchers.Main).launch {

            mBinding.btnContenue.animate().apply {
                alpha(0f)
                translationYBy(50f)
            }.withEndAction{
                mBinding.btnContenue.animate().apply{
                    duration = 500
                    alpha(1f)
                    translationYBy(-50f)
                }
            }.start()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }


}