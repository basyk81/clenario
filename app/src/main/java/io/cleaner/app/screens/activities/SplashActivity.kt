package io.cleaner.app.screens.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import io.cleaner.app.BaseActivity
import io.cleaner.app.databinding.ActivitySplashBinding
import io.cleaner.app.sharedpref.SharedPref
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.NullPointerException

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity() {

    private var binding: ActivitySplashBinding ?= null
    private val mBinding get() = binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {
        }
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        var isStartCheck = SharedPref.getValue(this)

        CoroutineScope(Dispatchers.Main).launch {
            startAnim()
            delay(2000)

            if(!isStartCheck){
                val intent = Intent(this@SplashActivity, OnboardingActivity::class.java)
                startActivity(intent)
                finish()
                //SharedPref.setValue(this@SplashActivity, true)
            }else{
                val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun startAnim() {
        CoroutineScope(Dispatchers.Main).launch {
            mBinding.tvWelcome.animate().apply {
                alpha(0f)
                translationYBy(50f)
            }.withEndAction{
                mBinding.tvWelcome.animate().apply{
                    duration = 1000
                    alpha(1f)
                    translationYBy(-50f)
                }
            }.start()
            delay(300)

            mBinding.tvName.animate().apply {
                alpha(0f)
                translationYBy(50f)
            }.withEndAction{
                mBinding.tvName.animate().apply{
                    duration = 1000
                    alpha(1f)
                    translationYBy(-50f)
                }
            }.start()
            delay(300)
            mBinding.tvClean.animate().apply {
                alpha(0f)
                translationYBy(50f)
            }.withEndAction{
                mBinding.tvClean.animate().apply{
                    duration = 1000
                    alpha(1f)
                    translationYBy(-50f)
                }
            }.start()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}