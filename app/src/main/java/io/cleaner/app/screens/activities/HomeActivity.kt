package io.cleaner.app.screens.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.builders.footer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.badgeable.secondaryItem
import co.zsmb.materialdrawerkt.draweritems.divider
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import io.cleaner.app.APP_HOME
import io.cleaner.app.BaseActivity
import io.cleaner.app.R
import io.cleaner.app.databinding.ActivityHomeBinding
import io.cleaner.app.databinding.ActivityMainBinding
import java.lang.NullPointerException

class HomeActivity : BaseActivity() {

    private var binding: ActivityHomeBinding?= null
    private val mBinding get() = binding!!
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {
        }
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        APP_HOME = this
        navController = Navigation.findNavController(this, R.id.nav_host_fragment_home)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}