package io.cleaner.app.sharedpref

import android.content.Context
import android.content.SharedPreferences

class SharedPref {
    companion object{
        fun setValue(context: Context, value: Boolean){
            val sharedPref: SharedPreferences = context.getSharedPreferences("test", Context.MODE_PRIVATE)
            val editor:SharedPreferences.Editor = sharedPref.edit()
            editor.putBoolean("saveValue", value)
            editor.apply()
        }
        fun getValue(context: Context):Boolean{
            val sharedPref: SharedPreferences = context.getSharedPreferences("test", Context.MODE_PRIVATE)
            return sharedPref.getBoolean("saveValue", false)
        }
    }
}