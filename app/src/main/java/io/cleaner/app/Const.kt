package io.cleaner.app

import io.cleaner.app.screens.activities.HomeActivity
import io.cleaner.app.screens.activities.MainActivity
import io.cleaner.app.screens.activities.OnboardingActivity

lateinit var APP_HOME: HomeActivity
lateinit var APP_MAIN: MainActivity
lateinit var APP_ONBOARDING: OnboardingActivity
const val REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 1