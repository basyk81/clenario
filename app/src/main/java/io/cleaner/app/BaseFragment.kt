package io.cleaner.app

import android.content.Context
import androidx.fragment.app.Fragment
import android.widget.Toast


open class BaseFragment : Fragment() {

    fun toast(text: String){
        Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
    }
    var ctx: Context?= null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ctx = context
    }
}