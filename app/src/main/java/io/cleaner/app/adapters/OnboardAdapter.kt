package io.cleaner.app.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import io.cleaner.app.screens.fragments.onboarding.FirstFragment
import io.cleaner.app.screens.fragments.onboarding.SecondFragment
import io.cleaner.app.screens.fragments.onboarding.ThreeFragment

class OnboardAdapter(fragmentActivity: FragmentActivity): FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> FirstFragment()
            1 -> SecondFragment()
            else -> ThreeFragment()

        }
    }
}